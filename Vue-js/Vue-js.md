## Vue.js



My cheat sheet from the [Vue.js for Beginners playlist](https://scrimba.com/p/playlist-38/), it's basically a copy/paste of the code from each chapter with a few notes and small changes.



[TOC]





## Hello world



```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
	</head>
	<body>

		<div id="app">
            {{ message }}
		</div>

		<script>
            var app = new Vue({
              el: '#app',
              data: {
                message: 'Hello Vue!'
              }
            })
        </script>

	</body>
</html>
```





## v-bind



```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
	</head>
	<body>

		<div id="app">
          <button v-bind:disabled="disableButton">Button</button>
        </div>

		<script>
            var app = new Vue({
              el: '#app',
              data: {
                disableButton: true
              }
            })
        </script>

	</body>
</html>
```



![](imgs/v-bind.PNG)





## v-if



```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
	</head>
	<body>

		<div id="app">
			<p v-if="seen">Now you see me</p>
		</div>

		<script>
            var app = new Vue({
				el: '#app',
				data: {
					seen: true
				}
			})
        </script>

	</body>
</html>
```



![](imgs/v-if.PNG)





## v-for



```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
	</head>
	<body>

		<div id="app">
		<ol>
			<li v-for="todo in todos">
				{{ todo.text }}
			</li>
		</ol>
		</div>

		<script>
			var app = new Vue({
				el: '#app',
				data: {
					todos: [
						{ text: 'Learn JavaScript' },
						{ text: 'Learn Vue' },
						{ text: 'Build something awesome' },
					]
				}
			})
			
			app.todos.push({ text: 'New item' })
        </script>

	</body>
</html>
```



![](imgs/v-for.PNG)





## v-on event listeners and methods



```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
	</head>
	<body>

		<div id="app">
			<p>{{ message }}</p>
			<button v-on:click="reverseMessage">Reverse Message</button>
		</div>

		<script>
			var app = new Vue({
				el: '#app',
				data: {
					message: 'Hello Vue.js!'
				},
				methods: {
					reverseMessage: function () {
						this.message = this.message.split('').reverse().join('')
					}
				}
			})
        </script>

	</body>
</html>
```



![](imgs/v-on-event-listeners-and-methods.PNG)





## v-model



```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
	</head>
	<body>

		<div id="app">
			<p>{{ message }}</p>
			<input v-model="message">
		</div>

		<script>
			var app = new Vue({
				el: '#app',
				data: {
					message: 'Hello Vue!'
				}
			})
        </script>

	</body>
</html>
```



![](imgs/v-model.PNG)





## Components



```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
	</head>
	<body>

		<div id="app">
			<my-component></my-component>
		</div>

		<script>
			Vue.component('my-component', {
				template: '<div>This is a Vue.js component!</div>'
			})

			new Vue({
				el: '#app'
			})
        </script>

	</body>
</html>
```



![](imgs/components.PNG)





## Components props



```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
	</head>
	<body>

		<div id="app">
			<my-component v-bind:message="msg"></my-component>
		</div>

		<script>
			Vue.component('my-component', {
				props: ['message'],
				template: `<div>{{ message }}</div>`,
			})

			var app = new Vue({
				el: '#app',
				data: {
					msg: 'Hello from Vue instance'
				}
			})
        </script>

	</body>
</html>
```



![](imgs/components-props.PNG)





## Components data



```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
	</head>
	<body>

		<div id="app">
			<my-component></my-component>
		</div>

		<script>
			Vue.component('my-component', {
				template: `
					<div>
						<div>{{ message }}</div>
						<button @click="handleMessageChange">Click to change message</button>
					</div>
				`,
                  // data in components are functions so each component instance can change data independently
				data: function() {
					return {
						message: 'Hello from Vue data!'
					}
				},
				methods: {
					handleMessageChange: function(){
						this.message = 'Message has been changed!'
					}
				}
			})

			new Vue({
				el: '#app'
			})
        </script>

	</body>
</html>
```



![](imgs/components-data.PNG)

