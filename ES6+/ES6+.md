# ES6+



My cheat sheet from [scrimba ES6+ course](https://scrimba.com/g/gintrotoes6), it's basically a copy/paste of the code from each chapter with a few notes and small changes.



[TOC]





## Template Literals:



```javascript
let word1 = 'Dylan';
let word2 = 'Israel';

const fullName = `${word1} ${word2}`;

// Dylan Israel
console.log(fullname)
```





## Destructuring Objects:



```javascript
const personalInformation = {
    firstName: 'Dylan',
    lastName: 'Israel',
    city: 'Austin',
    state: 'Texas',
    zipCode: 73301
};

const {firstName: fn, lastName: ln} = personalInformation;

// Dylan Israel
console.log(`${fn} ${ln}`);
```





## Destructuring Arrays



```javascript
let [firstName, middleName, lastName] = ['Dylan', 'Coding God', 'Israel'];

lastName = 'Clements';

// Clements
console.log(lastName)
```





## Object Literal



```javascript
function addressMaker(city, state) {
    const newAdress = {city, state};
    
    // {city: "Austin", state: "Texas"}
    console.log(newAdress);
}

addressMaker('Austin', 'Texas');
```





## For of Loop



Works wih iterables (Arrays, Strings, Maps, Sets etc...), not meant to change values.



```javascript
let fullName = "abc";

// a
// b
// c
for (const char of fullName) {
    console.log(char);
}

let incomes = [62000, 67000, 75000];

// /!\ Won't work /!\
for (let income of incomes) {
   income += 5000;
}

// [62000, 67000, 75000]
console.log(incomes);
```





## Spread Operator



```javascript
// reminder:

let example1 = [1];
let example2 = example1;
example2.push(2);

// [1, 2]
console.log(example1);
```



```javascript
let example1 = [1];
let example2 = [...example1];
example2.push(2);

// [1]
console.log(example1);

// [1, 2]
console.log(example2);
```





## Rest Operator



```javascript
function add(...nums) {
    
    // [4, 5, 7, 8, 12]
    console.log(nums);
}

add(4, 5, 7, 8, 12)
```





## Arrow Functions



```javascript
function add(...nums) {
    let total = nums.reduce((x, y) => x + y);
    
    // 36
    console.log(total);
}

add(4, 5, 7, 8, 12)
```





## Default params



```javascript
function add(numArray = []) {
    let total = 0;
    numArray.forEach((element) => {
        total += element;
    });
    
    // 0
    console.log(total);
}

add();
```





## includes()



```javascript
let numArray = [1,2,3,4,5];

// 1
console.log(numArray.indexOf(2));

// true
console.log(numArray.includes(2));
```





## Let & Const



```javascript
// reminder:

if (false) {
    var example = 5;
}

// null because of JavaScript's default behavior of moving declarations to the top (hoisting).
console.log(example)
```



```javascript
if (false) {
    let example = 5;
}

// Undefined error
console.log(example)
```



```javascript
const primitive = 5;

// error because const is read only for primitives
primitive = 4;

const example = [];
example.push(5)

// [5]
console.log(example)
```





## Import & Export



```javascript
// example.js
export const data = [1,2,3];
```



```javascript
// index.js
import { data } from './example.js';
let updatedData = data;
updatedData.push(5)

// [1, 2, 3, 5]
console.log(updatedData);
```





## padStart() & padEnd()



```javascript
let example = 'Dylan';
let example2 = 'Dylan Israel';

// aaaaaDylan
console.log(example.padStart(10, 'a'));

// "     Dylan"
'Dylan'.padStart(10);

// "Dylan"
'Dylan'.padStart(1);

// Dylanaaaaa
console.log(example.padEnd(10, 'a'));

// Dylan Israel
console.log(example2.padEnd(10, 'a'));
```





## Classes



```javascript
class Animal {
    constructor(type, legs) {
        this.type = type;
        this.legs = legs;
    }
    
    makeNoise(sound = 'Loud Noise') {
        console.log(sound);
    }
    
    get metaData() {
        return `Type: ${this.type}, Legs: ${this.legs}`;
    }
    
    // Doesn't requiere the class to be instantiated to be called
    static return10() {
        return 10;
    }
}

class Cat extends Animal {
    makeNoise(sound = "meow") {
        console.log(sound);
    }
}

// Object { type: "Cat", legs: 4 }
console.log(new Cat('Cat', 4));

// 10
console.log(Animal.return10());
```





## Async & Await



```javascript
const apiUrl = 'https://fcctop100.herokuapp.com/api/fccusers/top/alltime';

function getTop100Campers() {
    fetch(apiUrl)
    .then((r) => r.json())
    .then((json) => {
        console.log(json[0])
    }).catch((error) =>{
        console.log('failed');
    });
}
```



```javascript
const apiUrl = 'https://fcctop100.herokuapp.com/api/fccusers/top/alltime';

async function getTop100Campers() {
    const response = await fetch(apiUrl);
    const json = await response.json();
    
    console.log(json[0]);
}
```



```javascript
function resolveAfter3Seconds() {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve('resolved');
    }, 3000);
  });
}

// resolveAfter3Seconds().then((data) => {
//     console.log(data);
// });

async function getAsyncData() {
    const result = await resolveAfter3Seconds();
    console.log(result);
}

getAsyncData();
```





## Sets



```javascript
const exampleSet = new Set([1,1,1,1,2,2,2,2]);

exampleSet.add(5);
exampleSet.add(5).add(17);

// [ 1, 2, 5, 17 ]
console.log(exampleSet);

// 4
console.log(exampleSet.size);

// true
console.log(exampleSet.delete(17));

// true
console.log(exampleSet.has(5));

exampleSet.clear();

// []
console.log(exampleSet);
```
