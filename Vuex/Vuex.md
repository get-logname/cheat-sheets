# Vuex



My cheat sheet from the [scrimba Learn Vuex course](https://scrimba.com/g/gvuex), it's basically a copy/paste of the code from each chapter with a few notes and small changes.



CSS styles are very basic in this course and therefore will be ignored.



[TOC]





## Getting started



```html
<html>
    <body>
        <div id="app">
            {{message}}
        </div>
        <script src="index.pack.js"></script>
    </body>
</html>
```



```javascript
// index.js

import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        count: 0
    },
    mutations: {
        increment (state) {
            state.count++
        }
    }
});

store.commit('increment');

// 1
console.log(store.state.count);

new Vue({ 
    el: '#app',
    data: {
        message: 'Hello world'
    }
});
```





## Create Vuex Store and use it to hold state



```html
<html>
    <body>
        <div id="app">
            {{ count }}
        </div>
        <script src="index.pack.js"></script>
    </body>
</html>
```



```javascript
// index.js

import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        count: 0
    }
});

new Vue({ 
    el: '#app',
    // inject the store in this instance and all child components
    store,
    computed: {
        count () {
            return this.$store.state.count
        }
    }
});
```



![](imgs/create-vuex-store-and-use-it-to-hold-state.PNG)





## Create computed properties with Vuex mapState



```html
<html>
    <body>
        <div id="app">
            {{count}}
        </div>
        <script src="index.pack.js"></script>
    </body>
</html>
```



```javascript
// index.js

import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        count: 3
    }
});

import { mapState } from 'vuex';

new Vue({ 
    el: '#app',
    store,
    data() {
        return {
            localCount: 4
        }
    },
    computed: mapState([
        'count',
    ])
});
```



![](imgs/create-computed-properties-with-vuex-mapstate.PNG)





## Retrieve data from Vuex Store with Getters



```html
<html>
    <body>
        <div id="app">
            Completed Todos: {{ doneTodosCount }}
        </div>
        <script src="index.pack.js"></script>
    </body>
</html>
```



```javascript
// index.js

import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
   state: {
       todos: [
           { id: 1, text: '...', done: true },
           { id: 2, text: '...', done: false },
       ]
   },
   getters: {
       doneTodos: state => {
           return state.todos.filter(todo => todo.done);
       },
       doneTodosCount: (state, getters) => {
           return getters.doneTodos.length
       },
       getTodoById: (state) => (id) => {
           return state.todos.find(todo => todo.id === id)
       }
   }
});

import { mapGetters } from 'vuex';

new Vue({ 
    el: '#app',
    store,
    data: {
    },
    computed: mapGetters([
        'doneTodos',
        'doneTodosCount',
        'getTodoById',
    ])
});

// [{id: 1, text: "...", done: true}]
console.log(store.getters.doneTodos)

// [{id: 2, text: '...', done: false}]
console.log(store.getters.getTodoById(2))
```



![](imgs/retrieve-data-from-vuex-store-with-getters.PNG)





## Update state in Vuex Store with Mutations



```html
<html>
    <body>
        <div id="app">
            <button @click='decrement'>-</button>
            {{count}}
            <button @click='increment'>+</button>
        </div>
        <script src="index.pack.js"></script>
    </body>
</html>
```



```javascript
// index.js

import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        count: 0
    },
    mutations: {
        increment (state) {
            state.count++
        },
        incrementBy (state, payload) {
            state.count += payload.amount
        },
        decrement (state) {
            state.count--
        },
    }
});

import { mapState, mapMutations } from 'vuex';

new Vue({ 
    el: '#app',
    store,
    data: {
    },
    computed: mapState([
        'count'
    ]),
    methods: mapMutations([
        'increment',
        'incrementBy',
        'decrement'
    ])
});

// Vue.set(obj, 'new prop', 123)
// state.obj = { ...state.obj, newProp: 123 }
```



![](imgs/update-state-in-vuex-store-with-mutations.PNG)





## Update state asynchronously with Vuex Actions



```html

```



```javascript
// index.js


```





## Create Vuex Store and use it to hold state



```html

```



```javascript
// index.js


```





## Create Vuex Store and use it to hold state



```html

```



```javascript
// index.js


```





## Create Vuex Store and use it to hold state



```html

```



```javascript
// index.js


```





## Create Vuex Store and use it to hold state



```html

```



```javascript
// index.js


```





## Create Vuex Store and use it to hold state



```html

```



```javascript
// index.js


```





