# CSS Grid



My cheat sheet from the [scrimba CSS Grid course](https://scrimba.com/g/gR8PTE), it's basically a copy/paste of the code from each chapter with a few notes and small changes.



[TOC]



```css
/* basic.css */

html, body {
  background-color: #ffeead;
  margin: 10px;
}

.container > div {
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 2em;
    color: #ffeead;
}

.container > div > img {
    width: 100%;
    height: 100%;
    object-fit: cover;
}

.container > div:nth-child(1n) {
  background-color: #96ceb4;	
}

.container > div:nth-child(3n) {
  background-color: #88d8b0;
}

.container > div:nth-child(2n) {
      background-color: #ff6f69;
}

.container > div:nth-child(4n) {
      background-color: #ffcc5c;
}
```





## Basics



```html
<html>
    <head>
        <link rel="stylesheet" href="index.css">
        <link rel="stylesheet" href="basic.css">
    </head>
    <body>
        <div class="container">
            <div>1</div>
            <div>2</div>
            <div>3</div>
            <div>4</div>
            <div>5</div>
            <div>6</div>
        </div>
    </body>
</html>
```



```css
/* index.css */

.container {
    display: grid;
    grid-template-rows: 50px 50px 200px;
    grid-template-columns: 100px auto;
    grid-gap: 3px;
}
```



![](imgs/basics.PNG)





## Fraction unit, repeat() & shorthand



```css
/* index.css */

.container {
    display: grid;
    
    /*
    grid-template-rows: repeat(2, 50px);
    grid-template-columns: repeat(3, 1fr);
    */
    grid-template: repeat(2, 50px) / repeat(3, 1fr);
    grid-gap: 3px;
}
```



![](imgs/shorthand.PNG)





## Positioning items



```html
<html>
    <head>
        <link rel="stylesheet" href="index.css">
        <link rel="stylesheet" href="basic.css">
    </head>
    <body>
        <div class="container">
            <div class="header">HEADER</div>
            <div class="menu">MENU</div>
            <div class="content">CONTENT</div>
            <div class="footer">FOOTER</div>
        </div>
    </body>
</html>
```



```css
/* index.css */

.container {
    display: grid;
    grid-gap: 3px;
    grid-template: 40px 200px 40px / repeat(12, 1fr);
}

.header {
    /*
    grid-column- references the column lines (2 columns → 3 lines)
    grid-column-start: 1;
    grid-column-end: 3;
    */
    grid-column: 2 / 3;
    
    /* span on 2 columns */
    grid-column: 2 / span 2;
    
    /* -1 references the last column line */
    grid-column: 2 / -1;
}

.menu {
    grid-row: 1 / 3;
}

.content {
    grid-column: 2 / -1;
}

.footer {
    grid-column: 1 / -1;
}
```



![](imgs/positioning.PNG)





## Template areas



```css
/* index.css */

.container {
    height: 100%;
    display: grid;
    grid-gap: 3px;
    
    /* auto will make the row take all the available space with the container height at 100%  */
    grid-template-rows: 40px auto 40px;
    
    grid-template-columns: repeat(12, 1fr);
    
    /* visual representation of the template, must match the grid-template-columns */
    grid-template-areas: 
        ". h h h h h h h h h h ."
        "m c c c c c c c c c c c"
        ". f f f f f f f f f f ."
        ;
}

.header {
    grid-area: h;
}

.menu {
    grid-area: m;
}

.content {
    grid-area: c;
}

.footer {
    grid-area: f;
}
```



![](imgs/template-areas.PNG)





## Auto-fit and minmax



```html
<html>
    <head>
        <link rel="stylesheet" href="index.css">
    </head>
    <body>
        <div class="container">
            <div>1</div>
            <div>2</div>
            <div>3</div>
            <div>4</div>
            <div>5</div>
            <div>6</div>
            <div>7</div>
            <div>8</div>
            <div>9</div>
            <div>10</div>
            <div>11</div>
            <div>12</div>
        </div>
    </body>
</html>
```



```css
/* index.css */

.container {
    display: grid;
    grid-gap: 5px;
    grid-template-columns: repeat(auto-fit, minmax(100px, 1fr));
    grid-template-rows: repeat(2, 100px);
}
```



![](imgs/auto-fit-minmax.PNG)





## Implicit rows



```css
.container {
    display: grid;
    grid-gap: 5px;
    grid-template-columns: repeat(auto-fill, minmax(100px, 1fr));
    /* grid-template-rows: 100px 100px 100px; */
    grid-auto-rows: 100px;
}
```



![](imgs/implicit-rows.PNG)





## Image grid example



```html
<html>
    <head>
        <link rel="stylesheet" href="index.css">
        <link rel="stylesheet" href="basic.css">
    </head>
    <body>
        <div class="container">
            <div><img src="img/normal1.jpg"/></div>
            <div class="vertical"><img src="img/vertical1.jpg"/></div>
            <div class="horizontal"><img src="img/horizontal1.jpg"/></div>
            <div><img src="img/normal2.jpg"/></div>
            <div><img src="img/normal3.jpg"/></div>
            <div class="big"><img src="img/big1.jpg"/></div>
            <div><img src="img/normal4.jpg"/></div>
            <div class="vertical"><img src="img/vertical2.jpg"/></div>
            <div><img src="img/normal5.jpg"/></div>
            <div class="horizontal"><img src="img/horizontal2.jpg"/></div>
            <div><img src="img/normal6.jpg"/></div>
            <div class="big"><img src="img/big2.jpg"/></div>
            <div><img src="img/normal7.jpg"/></div>
            <div class="horizontal"><img src="img/horizontal3.jpg"/></div>
            <div><img src="img/normal8.jpg"/></div>
            <div class="big"><img src="img/big3.jpg"/></div>
            <div><img src="img/normal9.jpg"/></div>
            <div class="vertical"><img src="img/vertical3.jpg"/></div>
        </div>
    </body>
</html>
```



```css
.container {
    display: grid;
    grid-gap: 5px;
    grid-template-columns: repeat(auto-fit, minmax(100px, 1fr));
    grid-auto-rows: 75px;
    
    /* row by default */
    grid-auto-flow: dense;
}

.horizontal {
    grid-column: span 2;
}

.vertical {
    grid-row: span 2;
}

.big {
    grid-column: span 2;
    grid-row: span 2;
}
```



![](imgs/image-grid.PNG)





## Named lines



```html
<html>
    <head>
        <link rel="stylesheet" href="index.css">
        <link rel="stylesheet" href="basic.css">
    </head>
    <body>
        <div class="container">
            <div class="header">HEADER</div>
            <div class="menu">MENU</div>
            <div class="content">CONTENT</div>
            <div class="footer">FOOTER</div>
        </div>
    </body>
</html>
```



```css
.container {
    height: 100%; 
    display: grid;
    grid-gap: 3px;
    grid-template-columns: [main-start] 1fr [content-start] 5fr [content-end main-end];
    grid-template-rows: [main-start] 40px [content-start] auto [content-end] 40px [main-end]; 
}

.header {
    /* grid-column: main-start / main-end; */
    grid-column: main;
}

.menu {}

.content {
    /* grid-column: content-start / content-end; */
    /* grid-column: content; */
    
    /* works because the content is "boxed" in the container style, not possible with the footer, for example */
    grid-area: content;
}

.footer {
    /* grid-column: main-start / main-end; */
    grid-column: main;
}
```

 

![](imgs/named-lines.PNG)





## justify-content & align-items



```html
<html>
    <head>
        <link rel="stylesheet" href="index.css">
        <link rel="stylesheet" href="basic.css">

    </head>
    <body>
        <div class="container">
            <div>1</div>
            <div>2</div>
            <div>3</div>
            <div>4</div>
            <div>5</div>
            <div>6</div>
        </div>
    </body>
</html>
```



```css
.container {
    border: 1px solid black;
    height: 100%;
    display: grid;
    grid-gap: 5px;
    grid-template-columns: repeat(3, 100px);
    grid-template-rows: repeat(2, 100px);
    justify-content: space-around;
    align-content: center;
}
```



![](imgs/justify-align.PNG)





## auto-fit vs auto-fill



```html
<html>
    <head>
        <link rel="stylesheet" href="index.css">
        <link rel="stylesheet" href="basic.css">

    </head>
    <body>
        <h1 class="title">AUTO-FIT</h1>
        <div class="container">
            <div>1</div>
            <div>2</div>
            <div>3</div>
            <div>4</div>
        </div>
        <h1 class="title">AUTO-FILL</h1>
        <div class="container2">
            <div>1</div>
            <div>2</div>
            <div>3</div>
            <div>4</div>
        </div>
    </body>
</html>
```



```css
.container {
    border: 1px solid black;
    display: grid;
    grid-gap: 5px;
    grid-template-columns: repeat(auto-fit, minmax(100px, 1fr));
    grid-template-rows: 100px 100px;
}

.container2 {
    border: 1px solid black;
    display: grid;
    grid-gap: 5px;
    grid-template-columns: repeat(auto-fill, minmax(100px, 1fr));
    grid-template-rows: 100px 100px;
}
```



![](imgs/fill-vs-fit.PNG)





## Real-world example



[Creating an article layout](https://scrimba.com/p/pWqLHa/cdp76sD)